<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'wordpress_api' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'd5pZ>FBaCl f41<Q(F-n>J JhGDi92 ?d0P8;6{fh9(R[6@,>hj&Y#Q%k29w]/_!' );
define( 'SECURE_AUTH_KEY',  '~:,z]:4Gv+*/`#-Mrw;X=r#KEaYH?JtL~X2^0%tHn_O1,< 88$T@Vlzh&+[bg:oq' );
define( 'LOGGED_IN_KEY',    '%2}W%GH;G]I<ml3Jw_Z3]NEgm${1HS_g*[l*t?w[XJXe[&^yY;_5lO^~hMSmoqSS' );
define( 'NONCE_KEY',        'E UqJz[F^lkF=vp5(@R342,OIWt$>!~L50ETd(n}g$Ne0N(n4_qK<31k@m([Fd!7' );
define( 'AUTH_SALT',        'A/nl9<gVp<&fkentYh!DDXu/g@3efsMW7TRhCs>-+O~_5flB.?_ACYZ2bO^8-W^o' );
define( 'SECURE_AUTH_SALT', 'SZto/JhyeZU=P<k$Q+$x[|A89l#&h#4&zHd`quxASYLY`nk3,[M)*TJ|*tNq0t$;' );
define( 'LOGGED_IN_SALT',   'uLUUlUOIp ep!x,fZ a_34$6$I5C$;#xXcBI`-i@UF8!,T=i[)VBzZ%-}8:./RX{' );
define( 'NONCE_SALT',       'uN9TYGU$cPF0sI:=mzN(_SkE~-Yq0IXS^*:.d^7_ny<sCU2cOJZ-TaS&)`{orI#M' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
